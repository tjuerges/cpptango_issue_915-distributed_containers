# Testkit for cppTango event subscription across containers on different hosts

## Prerequisites

You will need two systems. That can be two computers  or two VMs or the same computer with two separate docker networks. What is important:

- The two systems need to be independent network nodes and not share the same host IP address.
- The two systems can reach each other. For instance they can `ping` each other.

I will refer to the two systems as `computer1` and `computer2`.

You need the following to run the provided `docker-compose` set up:

- Docker
- Docker compose

## Do this

- Clone this repo.

Then:

- For `computer1`:

    - Just start `runme.sh`. **It is important that you run the script from its directory!** 

        ```bash
        cd computer1
        ./runme.sh
        ```

    - You can stop the program at any time by pressing `CTRL-C`.

- For `computer2`:

    - Just start the `runme.sh` script like this:
        ```bash
        cd computer2
        TANGO_HOST=foo.bar:10000 ./runme.sh
        ```
        
        This will tell the `runme.sh` script what `TANGO_HOST` to use. Here I use `foo.bar:10000` but you need to provide the correct FQDN. **It is important that you run the script from its directory!**
        
    - Or edit  `computer2/runme.sh` and enter the `FQDN` of `computer1` where it says `ENTER_FQDN_OF_COMPUTER1_HERE`. 
    
        ```bash
        cd computer2
        cat runme.sh
        #! /usr/bin/env bash
        
        export TANGO_HOST=${TANGO_HOST:=ENTER_HERE_FQDN_OF_COMPUTER1:10000}
        
        # Sanity check if TANGO_HOST was actually modified.
        if [ ${TANGO_HOST} = ENTER_HERE_FQDN_OF_COMPUTER1:10000 ]; then
            echo -e "\n*** ERROR! You need to set TANGO_HOST in this file!\n"
            exit 0
        fi
        
        docker network create computer2
        docker-compose up
        docker-compose down
        docker network rm computer2
        ```
    
    - Then you can start `runme.sh` without prefixing it with `TANGO_HOSTY`. **It is important that you run the script from its directory!**
        ```bash
        cd computer2
        ./runme.sh
        ```
    
    - The program on `computer2` will automatically stop after 60s.

## Output

### `computer1`

- The output on computer1 will look similar to this:
    ```bash
    thomas@okeanos computer1 (master) 90: ./runme.sh
    0e6d1f5dc325b13bd5f66cfaff96c8fbc283f883d330dbc7a508b638a9054f7f
    [+] Running 5/5
     ⠿ Network computer1_dockernet  Created                                                                                              0.1s
     ⠿ Container tangodb            Created                                                                                              0.1s
     ⠿ Container databaseds         Created                                                                                              0.1s
     ⠿ Container dsconfig           Created                                                                                              0.1s
     ⠿ Container computer1          Created                                                                                              0.1s
    Attaching to computer1, databaseds, dsconfig, tangodb
    tangodb     | 2022-05-12 18:40:26+00:00 [Note] [Entrypoint]: Entrypoint script for MariaDB Server 1:10.7.3+maria~focal started.
    tangodb     | 2022-05-12 18:40:26+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
    tangodb     | 2022-05-12 18:40:26+00:00 [Note] [Entrypoint]: Entrypoint script for MariaDB Server 1:10.7.3+maria~focal started.
    tangodb     | 2022-05-12 18:40:27+00:00 [Note] [Entrypoint]: MariaDB upgrade not required
    tangodb     | 2022-05-12 18:40:27 0 [Note] mariadbd (server 10.7.3-MariaDB-1:10.7.3+maria~focal) starting as process 1 ...
    tangodb     | 2022-05-12 18:40:27 0 [Note] Plugin 'InnoDB' is disabled.
    tangodb     | 2022-05-12 18:40:27 0 [Note] Plugin 'FEEDBACK' is disabled.
    tangodb     | 2022-05-12 18:40:27 0 [Warning] You need to use --log-bin to make --expire-logs-days or --binlog-expire-logs-seconds work.
    tangodb     | 2022-05-12 18:40:27 0 [Note] Server socket created on IP: '0.0.0.0'.
    tangodb     | 2022-05-12 18:40:27 0 [Note] Server socket created on IP: '::'.
    tangodb     | 2022-05-12 18:40:27 0 [Note] mariadbd: ready for connections.
    tangodb     | Version: '10.7.3-MariaDB-1:10.7.3+maria~focal'  socket: '/run/mysqld/mysqld.sock'  port: 3306  mariadb.org binary distribution
    databaseds  | wait-for-it.sh: waiting 30 seconds for tangodb:3306
    tangodb     | 2022-05-12 18:40:27 3 [Warning] Aborted connection 3 to db: 'unconnected' user: 'unauthenticated' host: '172.26.0.3' (This connection closed normally without authentication)
    databaseds  | wait-for-it.sh: tangodb:3306 is available after 0 seconds
    databaseds  | main(): arrived
    databaseds  | main(): export DataBase as named servant (name=database)
    databaseds  | Ready to accept request
    dsconfig    | wait-for-it.sh: waiting 30 seconds for databaseds:10000
    dsconfig    | wait-for-it.sh: databaseds:10000 is available after 0 seconds
    dsconfig    |
    dsconfig    | *** No changes needed in Tango DB ***
    dsconfig exited with code 0
    computer1   | wait-for-it.sh: waiting 30 seconds for databaseds:10000
    computer1   | wait-for-it.sh: databaseds:10000 is available after 0 seconds
    databaseds  | device tango/admin/e84bd6805267 not defined in the database !
    databaseds  | device tango/admin/92eb53181133 not defined in the database !
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd1
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd2
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd3
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd4
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd5
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd6
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd7
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd8
    computer1   | 1652380829 [139819036440320] ERROR tango/randomdata/1 MIN ALARM for attribute rnd9
    ```

- If it does not look like this, then press `CTRL-C` to stop the containers. Sometimes the interplay between the containers does not work as expected. Just start `./runme.sh` again.

### `computer2`

- Watch the output on `computer2`.

    - The issue can be seen if the output looks like this:
        ```bash
        thomas@okeanos computer2 (master) 2: TANGO_HOST=okeanos:10000 ./runme.sh
        d5472d3c97b8823181980ad5ce337bbf51557f67c72e38c1ae7613b5377596f7
        [+] Running 2/2
         ⠿ Network computer2_default  Created                                                                                                0.1s
         ⠿ Container computer2        Created                                                                                                0.1s
        Attaching to computer2
        computer2  | wait-for-it.sh: waiting 30 seconds for okeanos:10000
        computer2  | wait-for-it.sh: okeanos:10000 is available after 0 seconds
        computer2  | *** Subscribing to events...
        computer2  | 2022-05-12T20:56:12.170 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.33577325912200706
        computer2  | *** Sleeping for 60s...
        computer2  | 2022-05-12T20:56:24.228 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, error = ((DevError(desc = 'Event channel is not responding anymore, maybe the server or event system is down', origin = 'EventConsumer::KeepAliveThread()', reason = 'API_EventTimeout', severity = tango._tango.ErrSeverity.ERR),))
        computer2  | 2022-05-12T20:56:34.268 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, error = ((DevError(desc = 'Event channel is not responding anymore, maybe the server or event system is down', origin = 'EventConsumer::KeepAliveThread()', reason = 'API_EventTimeout', severity = tango._tango.ErrSeverity.ERR),))
        computer2  | 2022-05-12T20:56:44.299 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, error = ((DevError(desc = 'Event channel is not responding anymore, maybe the server or event system is down', origin = 'EventConsumer::KeepAliveThread()', reason = 'API_EventTimeout', severity = tango._tango.ErrSeverity.ERR),))
        computer2  | 2022-05-12T20:56:54.363 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, error = ((DevError(desc = 'Event channel is not responding anymore, maybe the server or event system is down', origin = 'EventConsumer::KeepAliveThread()', reason = 'API_EventTimeout', severity = tango._tango.ErrSeverity.ERR),))
        computer2  | 2022-05-12T20:57:04.395 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, error = ((DevError(desc = 'Event channel is not responding anymore, maybe the server or event system is down', origin = 'EventConsumer::KeepAliveThread()', reason = 'API_EventTimeout', severity = tango._tango.ErrSeverity.ERR),))
        computer2  | *** Waking up and unsubscribing from events...
        computer2  | Unsubscribed from events. I am done. Good bye!
        computer2 exited with code 0
        [+] Running 2/0
         ⠿ Container computer2        Removed                                                                                                0.0s
         ⠿ Network computer2_default  Removed                                                                                                0.1s
        computer2
        ```
        
        The subscription is apparently successful, because on subscription the device immediately submits a change request which is successfully received. But subsequent events get lost, because the event channel does not supply events at all. 
        

### Expected output

This is how it should look like:

```bash
thomas@okeanos computer2 (master) 3: python3 client.py
*** Subscribing to events...
2022-05-12T20:56:53.383 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.9719486652486747
*** Sleeping for 60s...
2022-05-12T20:56:53.416 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.8866612407514753
2022-05-12T20:56:53.516 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.8265450899675464
2022-05-12T20:56:53.617 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.3726175051896068
2022-05-12T20:56:53.714 device = RandomData(tango/randomdata/1), attribute = tango://okeanos.senmut.net:10000/tango/randomdata/1/rnd10, event = change, value = 0.4994955745311359
```



## Alternative way

I noticed that running a `Jive` container on `computer2` also reproduces the issue.

- Just `docker pull` a `Jive` image on `computer2`.
- `docker run` the container.
-  In `Jive` set the `TANGO_HOST` to `FQDN_of_computer1:10000`.
- Then open the device server  `RandomData`.
- Start a monitor on `tango/RandomData/1`.
- You will see plenty of failed subscription logs.