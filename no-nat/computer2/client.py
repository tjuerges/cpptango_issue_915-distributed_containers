import tango
from time import sleep

def run_me() -> int:
#     sleep(86400)
    dp = tango.DeviceProxy('tango/RandomData/1')
    print('*** Subscribing to events...')
    id = dp.subscribe_event('rnd10', tango.EventType.CHANGE_EVENT, tango.utils.EventCallback())
    print('*** Sleeping for 15s...')
    sleep(15)
    print('*** Waking up and unsubscribing from events...')
    dp.unsubscribe_event(id)
    print('Unsubscribed from events. I am done. Good bye!')
    return 0

def main(args = None, **kwargs):
    return run_me()

if __name__ == '__main__':
    main()
