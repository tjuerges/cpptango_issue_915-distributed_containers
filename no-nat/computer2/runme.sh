#! /usr/bin/env bash

export TANGO_HOST=${TANGO_HOST:=ENTER_HERE_FQDN_OF_COMPUTER1:10000}

# Sanity check if TANGO_HOST was actually modified.
if [ ${TANGO_HOST} = ENTER_HERE_FQDN_OF_COMPUTER1:10000 ]; then
    echo -e "\n*** ERROR! You need to set TANGO_HOST in this file!\n"
    exit 0
fi

compose="docker-compose"
which ${compose} >&/dev/null
[[ ${?} -eq 1 ]] && compose="docker compose"

${compose} up
${compose} down
