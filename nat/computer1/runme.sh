#! /usr/bin/env bash

export HOSTNAME=$(hostname -f)
export TANGO_HOST=${HOSTNAME}:10000
export TANGO_CORBA_PORT=50000
export TANGO_ZMQ_HEARTBEAT_PORT=50001
export TANGO_ZMQ_EVENT_PORT=50002

compose="docker-compose"
which ${compose} >&/dev/null
[[ ${?} -eq 1 ]] && compose="docker compose"

${compose} up
${compose} down
